package com.example_kafka.example_kafka.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic testTopic(){
        return TopicBuilder.name("javaTest")
                .build();
    }

    @Bean
    public NewTopic testJsonTopic(){
        return TopicBuilder.name("javaTest_json")
                .build();
    }
}
